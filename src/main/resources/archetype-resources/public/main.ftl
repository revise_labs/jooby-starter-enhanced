<#macro meta></#macro>
<#macro syles></#macro>
<#macro body></#macro>
<#macro scripts></#macro>
<#macro styles></#macro>
<#macro json></#macro>
<#macro render title="MPU | Application Name">
<!doctype html>
<html lang="en" ng-app>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1" />
    <@meta />
    <link rel="stylesheet" href="/assets/dist/styles.css">
    <@styles />
    <title>${ title }</title>
</head>
<body>
    <script type="application/json" id="viewData"><@json /></script>
    <div class="page-wrapper">
        <div class="app-bar">

        </div>
        <div class="main-content">
            <@body />
        </div>
    </div>
    <div class="footer">

    </div>
    <script src="/assets/dist/scripts.js"></script>
    <@scripts />
</body>
</html>
</#macro>