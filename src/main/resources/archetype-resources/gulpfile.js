/**
 * Created by ksheppard on 22/04/2016.
 */
var gulp = require("gulp"),
    less = require("gulp-less"),
    zip =  require('gulp-zip'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    cleanCss = require('gulp-clean-css'),
	prefix = require('gulp-autoprefixer'),
	sourcemaps = require('gulp-sourcemaps');

var assets = {
    css: {
        base: 'public/assets/css'
    },
    js: {
        base: 'public/assets/js'
    },
    dist: {
        path: 'public/assets/dist',
        jsFile: 'scripts.js',
        cssFile: 'styles.css'
    }
};
assets.css.files = [
    //'node_modules/angular/angular-csp.css',
    assets.css.base + '/**/*.css'
];

assets.css.less = assets.css.base + '/**/*.less'; //or '/**/[^_]*.less' to exclude partials

// Place more js files here assets.js.base + '/path/to/file.js' or assets.js.base + '/**/*.js' Remember, order is important.
assets.js.files= [
    'node_modules/jquery/dist/jquery.js',
    'node_modules/angular/angular.js',
    assets.js.base + '/**/*.js'
];

/* Converts all *.less files to *.css files then concatenates all the *.css files into one file (public/assets/dist/styles.css) */
gulp.task('concat-css', ['less'], function() {
    return gulp.src(assets.css.files)
        .pipe(sourcemaps.init({loadMaps: true}))
        .pipe(concat(assets.dist.cssFile))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(assets.dist.path));
});

/* Concatenates all the *.js files into one file (public/assets/dist/scripts.js) */
gulp.task('concat-js', function() {
    return gulp.src(assets.js.files).pipe(sourcemaps.init())
        .pipe(concat(assets.dist.jsFile))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(assets.dist.path));
});

/* A simple task to concatenate all the assets for the first time. */
gulp.task('init', ['concat-css', 'concat-js']);

/* Converts *.less files into *.css files */
gulp.task('less', function() {
    return gulp.src(assets.css.less)
        .pipe(sourcemaps.init())
        .pipe(less().on('error', console.log))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(assets.css.base));
});

gulp.task('min-css', function () {
    return gulp.src(assets.css.less)
        .pipe(less().on('error', console.log))
        .pipe(concat(assets.dist.cssFile))
        .pipe(cleanCss())
        .pipe(gulp.dest(assets.dist.path));
});

/* Minifies all the assets to reduce file size */
gulp.task('minify-assets', ['min-js', 'min-css']);

/* Minifies the scripts.js file */
gulp.task('min-js', function() {
    return gulp.src(assets.js.files)
        .pipe(concat(assets.dist.jsFile))
        .pipe(uglify())
        .pipe(gulp.dest(assets.dist.path));
});

/* Watches all the *.less and *.css files for changes and runs the concat-css task */
gulp.task('watch-css', function() {
    gulp.watch(assets.css.base + "/**/*.less", ['concat-css']);
});

/* Watches all the *.js files for changes and runs the concat-js task */
gulp.task('watch-js', function() {
    gulp.watch(assets.js.base + "/**/*.js", ['concat-js']);
});
