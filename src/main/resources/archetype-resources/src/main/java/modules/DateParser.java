package ${package}.modules;

import com.google.inject.Binder;
import com.google.inject.TypeLiteral;
import com.google.inject.multibindings.Multibinder;
import com.typesafe.config.Config;
import org.jooby.Env;
import org.jooby.Jooby;
import org.jooby.Parser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Pulled from org.jooby.internal.parsers.DateParser class.
 * Returns null for unparsable dates instead of throwing an exception.
 * Created by Kevin on 6/29/2016.
 */
public class DateParser implements Jooby.Module {

    @Override
    public void configure(Env env, Config conf, Binder binder) {
        FriendlyDateParser parser = new FriendlyDateParser(conf.getString("application.dateFormat"));
        Multibinder.newSetBinder(binder, Parser.class).addBinding().toInstance(parser);
    }

    public class FriendlyDateParser implements Parser {
        Logger logger = LoggerFactory.getLogger(FriendlyDateParser.class);
        private SimpleDateFormat format;

        public FriendlyDateParser(String dateFormat) {
            format = new SimpleDateFormat(dateFormat);
        }

        @Override
        public Object parse(TypeLiteral<?> type, Context ctx) throws Throwable {
            if(type.getRawType() == Date.class) {
                return ctx.param(p -> {
                    logger.debug("Parsing parameter. {}", p.first());
                    return parseDate(p.first());
                })
                .body(b -> {
                    logger.debug("Parsing body. {}", b.text());
                    return parseDate(b.text());
                });
            } else return ctx.next();
        }

        public Date parseDate(String txtDate) {
            try {
                return format.parse(txtDate);
            } catch (ParseException e) { return null; }
        }
    }
}
