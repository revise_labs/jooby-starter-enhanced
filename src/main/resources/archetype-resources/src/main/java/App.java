package ${package};

import org.jooby.Jooby;
import org.jooby.Results;
import org.jooby.ftl.Ftl;
import org.jooby.json.Gzon;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import utils.FriendlyJsonDate;

import java.util.Date;

/**
 * @author jooby generator
 */
public class App extends Jooby {

  Logger logger = LoggerFactory.getLogger(App.class);

  {
    use(new Ftl("", ".ftl"));

    use(new Gzon().doWith((builder, config) ->
            builder.registerTypeAdapter(Date.class, new FriendlyJsonDate(config.getString("application.dateFormat")))
    ));

    assets("/assets/**");
//    assets("/favicon.ico", "/assets/images/favicon.png");

    before((req, res) -> {
      req.set("absoluteUrl", String.format("%s%s%s", req.secure() ? "https://" : "http://", req.hostname(), req.path()));
      req.set("host", String.format("%s%s", req.secure() ? "https://" : "http://", req.hostname()));
      req.set("hostName", req.hostname());
    });

    get("/", () -> Results.html("index"));
  }

  public static void main(final String[] args) throws Throwable {
    run(App::new, args);
  }

}
