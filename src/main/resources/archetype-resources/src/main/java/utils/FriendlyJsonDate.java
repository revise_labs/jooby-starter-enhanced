package ${package}.utils;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Kevin on 6/29/2016.
 */
public class FriendlyJsonDate implements JsonDeserializer<Date> {

    private SimpleDateFormat format;

    public FriendlyJsonDate(String dateFormat) {
        format = new SimpleDateFormat(dateFormat);
    }

    @Override
    public Date deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        try {
            return format.parse(jsonElement.getAsString());
        } catch (ParseException e) { return null; }
    }
}
