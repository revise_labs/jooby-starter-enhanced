package ${package}.utils;

import java.util.Random;
import java.util.stream.IntStream;

/**
 * Created by Kevin on 6/26/2016.
 */
public class Auth {

    private static char[] alphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789*$-_^@!".toCharArray();

    public static String generateAuthToken(int length) {
        StringBuilder token = new StringBuilder();
        IntStream.range(1,length+1).forEach(number -> {
            Random random = new Random();
            int index = random.nextInt(alphabet.length);
            token.append(alphabet[index]);
        });
        return token.toString();
    }

    public static String generateAuthToken() {
        return generateAuthToken(32);
    }

}
