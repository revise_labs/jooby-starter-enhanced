# Jooby CR5 Starter Template for IntelliJ IDEA

This template aims to start you off with the tools and libraries you'll normally need for a Jooby project in MPU.

Please change the following to suit your project's needs':

### pom.xml

```xml
  <properties>
      <!-- Startup class -->
      <application.class>App</application.class>
  </properties>
```

### Tools Included

* Freemarker Templates
* Jooby Gzon
* Revise Labs Database Tools
* Revise Labs Validation Tools
* Default Jooby libraries

### Before You Begin

Before you start running the application, be sure to run `npm install` then `gulp init`.

### Asset Pipeline

This project template uses Less for CSS pre-processing and Gulp as the task runner. Below are some
common tasks that you'll run when developing:

* watch-css - Watches all the *.less and *.css files for changes and runs the concat-css task
* watch-js - Watches all the *.js files for changes and runs the concat-js task
* concat-css - Converts all *.less files to *.css files then concatenates all the *.css files into one file (public/assets/dist/styles.css)
* concat-js - Concatenates all the *.js files into one file (public/assets/dist/scripts.js)

**IMPORTANT:** Before deploying to production it is very important that you minify all your JavaScript and CSS assets. To do that, run `gulp minify-assets`.

Still outstanding: CSS and main view template

Happy coding!

