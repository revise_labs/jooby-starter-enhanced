## Jooby Enhanced Starter Project

### Installation

1. Clone the project onto your local disk
```
git clone git@bitbucket.org:revise_labs/jooby-starter-enhanced.git
```
2. Install into your local repository

```
cd jooby-starter-enhanced
mvn install
```
3. Use the Archetype to create your project
```
mvn archetype:generate -DarchetypeGroupId=com.reviselabs \
                       -DarchetypeArtifactId=jooby-starter-enhanced \
                       -DarchetypeVersion=1.0.0 \
                       -DgroupId=com.example \
                       -DartifactId=mpy-project \
                       -Dversion=1.0 \
                       -DinteractiveMode=false
```

### Additional Settings

You can also specify the following settings when creating your project as a Java System Property (-D)

* compilerSource
* compilerTarget

Both are set to 1.8 by default to target Java 8 and the jdk-1.8 library.

Happy coding!



